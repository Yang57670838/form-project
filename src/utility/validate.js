export const isAccountNumber = (value) => {
    return /^\d{1,12}$/.test(value);
}