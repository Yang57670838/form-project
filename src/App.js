import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import theme from './styles/theme'
import Form from './components/pages/Form'
import Review from './components/pages/Review'
import Login from './components/pages/Login'
import Header from './components/organisms/Header'
import Logout from './components/pages/Logout'

function App() {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path='/' component={Form} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/review' component={Review} />
          <Route exact path='/logout' component={Logout} />
        </Switch>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
