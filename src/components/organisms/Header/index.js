import React, { useState, useEffect } from 'react'
import { Link, withRouter } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import useScrollTrigger from '@material-ui/core/useScrollTrigger'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import MenuIcon from '@material-ui/icons/Menu'
import Iconbutton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'

const useStyles = makeStyles(theme => ({
    toolbarMargin: {
        ...theme.mixins.toolbar,
        backgroundColor: 'red'
    },
    tabContainer: {
        marginLeft: 'auto'
    },
    tab: {
        ...theme.typography.tab,
        minWidth: 10,
        marginLeft: '25px'
    },
    appBar: {
        zIndex: theme.zIndex.modal + 1
    },
    button: {
        ...theme.typography.headerButton,
        borderRadius: '50px',
        marginLeft: '25px',
        marginRight: '25px',
        height: '45px'
    },
    drawerIconContainer: {
        marginLeft: 'auto',
        "&:hover": {
            backgroundColor: "transparent"
        }
    },
    drawerIcon: {
        height: '40px',
        weight: '40px'
    },
    drawerItem: {
        ...theme.typography.tab,
        opacity: 0.7
    },
    drawerItemSelected: {
        ...theme.typography.tab,
        opacity: 1
    },
    headerButton: {
        backgroundColor: '#E61778'
    },
    headerButtonText: {
        color: 'white',
        ...theme.typography.tab,
        opacity: 1
    },
    headerButtonTextSelected: {
        color: 'white',
        ...theme.typography.tab,
        opacity: 0.7
    }
}))


function ElevationScroll(props) {
    const { children } = props;

    const trigger = useScrollTrigger({
        disableHysteresis: true,
        threshold: 0
    });

    return React.cloneElement(children, {
        elevation: trigger ? 4 : 0
    });
}

const Header = props => {

    const classes = useStyles()
    const [currentTab, setTab] = useState(0)
    const [openDrawer, setOpenDrawer] = useState(false)
    const theme = useTheme()
    const smallScreenMatched = useMediaQuery(theme.breakpoints.down("md"))
    const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);

    // to do: remove review route from header later
    const tabRoutes = [
        {
            label: 'Home', path: '/',
        },
        {
            label: 'Review', path: '/review'
        }
    ]

    useEffect(() => {
        if (window.location.pathname === '/' && currentTab !== 0) {
            setTab(0)
        } else if (window.location.pathname === '/review' && currentTab !== 1) {
            setTab(1)
        }
    }, [])

    const handleTabChange = (e, value) => {
        setTab(value)
    }

    const tabs = (
        <React.Fragment>
            <Tabs
                className={classes.tabContainer}
                value={currentTab}
                onChange={handleTabChange}
                indicatorColor="primary"
            >
                {tabRoutes.map((r, i) => {
                    return (
                        <Tab key={`tab_${i}`} className={classes.tab} component={Link} to={r.path} label={r.label} />
                    )
                })}
            </Tabs>
            <Button variant="contained" color="secondary" className={classes.button} onClick={() => { }}>
                Logout
            </Button>
        </React.Fragment>
    )

    const drawer = (
        <React.Fragment>
            <SwipeableDrawer
                disableBackdropTransition={!iOS}
                disableDiscovery={iOS}
                open={openDrawer}
                onClose={() => setOpenDrawer(false)}
                onOpen={() => setOpenDrawer(true)}
            >
                <div className={classes.toolbarMargin} />
                <List disablePadding>
                    {tabRoutes.map((r, i) => {
                        return (
                            <ListItem
                                onClick={() => { setOpenDrawer(false); setTab(i) }}
                                divider
                                button
                                component={Link}
                                to={r.path}
                                selected={currentTab === i}
                                key={`list_tab_${i}`}
                            >
                                <ListItemText
                                    disableTypography
                                    className={currentTab === i ? classes.drawerItemSelected : classes.drawerItem}
                                >
                                    {r.label}
                            </ListItemText>
                            </ListItem>
                        )
                    })}
                    <ListItem
                        onClick={() => { setOpenDrawer(false); setTab(tabRoutes.length) }}
                        divider
                        button
                        component={Link}
                        to='/logout'
                        selected={currentTab === tabRoutes.length}
                        className={classes.headerButton}
                    >
                        <ListItemText
                            disableTypography
                            className={currentTab === tabRoutes.length ? classes.headerButtonText : classes.headerButtonTextSelected}
                        >
                            Logout
                        </ListItemText>
                    </ListItem>
                </List>
            </SwipeableDrawer>
            <Iconbutton className={classes.drawerIconContainer} onClick={() => setOpenDrawer(!openDrawer)} disableRipple>
                <MenuIcon color="secondary" className={classes.drawerIcon} />
            </Iconbutton>
        </React.Fragment>
    )
    // because we need to material ui click effect when click tabs, we have to put it inside the react-route
    // so we hide header for pages dont need header component
    // to do: think better solution
    if (window.location.pathname === '/login') return null;

    return (
        <React.Fragment>
            <ElevationScroll>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <Typography variant="h3">
                            aaa
                        </Typography>
                        {
                            smallScreenMatched ? drawer : tabs
                        }
                    </Toolbar>
                </AppBar>
            </ElevationScroll>
            <div className={classes.toolbarMargin} />
        </React.Fragment>
    );
}

export default withRouter(Header)