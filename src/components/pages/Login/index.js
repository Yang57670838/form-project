import React, { useState, useEffect } from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import InputAdornment from '@material-ui/core/InputAdornment';
import CheckIcon from '@material-ui/icons/Check';
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { makeStyles } from '@material-ui/core/styles'
import { green } from '@material-ui/core/colors';
import Checkbox from '@material-ui/core/Checkbox';
import ClearIcon from '@material-ui/icons/Clear';
import { isAccountNumber } from '../../../utility/validate'

const useStyles = makeStyles(theme => ({
    loginCard: {
        marginTop: '30vh'
    },
    paper: {
        padding: '50px'
    },
    button: {
        width: '100%'
    }
}))

const Login = () => {

    const [account, setAccount] = useState('')
    const [rememberMe, setRememberMe] = useState(false)
    const [accountErr, setAccountErr] = useState(null)

    const classes = useStyles()

    useEffect(() => {
        const savedFormID = localStorage.getItem('formID');
        if (savedFormID) {
            setAccount(savedFormID)
            setRememberMe(true)
        }
      }, [])

    const inputAccountHandler = (event) => {
        setAccount(event.target.value)
        if (isAccountNumber(event.target.value)) {
            setAccountErr(false)
        } else {
            setAccountErr(true)
        }
    }

    const rememberMeCheckedHandler = () => {
        setRememberMe(!rememberMe)
    }

    const handleLogin = () => {
        // to do: redux login
        // to do: remove save localstorage after success redux login action
        if (rememberMe) {
            localStorage.setItem('formID', account);
        } else {
            localStorage.removeItem('formID');
        }
    }

    return (
        <div className={classes.loginCard}>
            <Grid container spacing={0} justify="center" direction="row">
                <Grid item>
                    <Paper
                        variant="elevation"
                        elevation={2}
                        className={classes.paper}
                    >
                        <Grid
                            container
                            direction="column"
                            justify="center"
                            spacing={2}
                        >
                            <Grid item>
                                <Typography variant="h5">
                                    aaa
                                </Typography>
                            </Grid>
                            <Grid item>
                                <TextField
                                    id="accountID"
                                    label="Account Number"
                                    variant="outlined"
                                    type="number"
                                    error={accountErr}
                                    helperText={accountErr ? "App ID must be a number only" : ""}
                                    value={account}
                                    onChange={inputAccountHandler}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="end">
                                            {
                                                accountErr === null ? <></> : accountErr ? <ClearIcon color='error' /> : <CheckIcon style={{ color: green[500] }} />
                                            }
                                        </InputAdornment>
                                    }}
                                    required
                                />
                            </Grid>
                            <Grid item>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={rememberMe}
                                            onChange={rememberMeCheckedHandler}
                                            name="remembermeCheck"
                                            color="primary"
                                        />
                                    }
                                    label="Remember Form ID"
                                />
                            </Grid>
                            <Grid item>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    type="submit"
                                    className={classes.button}
                                    disabled={accountErr || !account}
                                    onClick={handleLogin}
                                >
                                    确认
                                </Button>
                            </Grid>
                        </Grid>
                    </Paper>
                </Grid>
            </Grid>
        </div>
    );
}

export default Login;