import { createMuiTheme } from '@material-ui/core/styles';

const seekBlue = "#133980"
const seekRed = "#E61778"
const bankAmountGreen = '#57A055'
const ANZPrimaryBlue = '#0E4165'

export default createMuiTheme({
    palette: {
      primary: {
        main: `${seekBlue}`,
      },
      secondary: {
        main: `${seekRed}`,
      },
    },
    typography: {
        tab: {
            textTransform: 'none',
            fontweight: 700,
            fontSize: '1rem',
        },
        headerButton: {
            fontSize: '1rem',
            textTransform: 'none',
            color: 'white'
        }
    }
  })