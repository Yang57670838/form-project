import React from 'react'
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { useTheme } from '@material-ui/core/styles';
import FormContent from '../FormContent'
import UploadDocuments from '../UploadDocuments'
import Review from '../Review'

const useStyles = makeStyles(theme => ({
    formContent: {
        minHeight: 'calc(100vh - 70px - 64px)'
    }
}))

// should getting from dynamic JSON driven data
const getSteps = () => {
    return ['客户信息', '上传资料', 'review'];
}

// should getting from dynamic JSON driven data
const getStepContent = (stepIndex) => {
    switch (stepIndex) {
        case 0:
            return <FormContent />;
        case 1:
            return <UploadDocuments />;
        case 2:
            return <Review />;
        default:
            return 'Unknown stepIndex';
    }
}

const StepperWrapper = () => {

    const classes = useStyles()
    const theme = useTheme()
    const smallScreenMatched = useMediaQuery(theme.breakpoints.down("md"))

    const [activeStep, setActiveStep] = React.useState(0);
    const steps = getSteps();

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleReset = () => {
        setActiveStep(0);
    };
    return (
        <>
            {!smallScreenMatched && (
                <Stepper activeStep={activeStep} alternativeLabel>
                    {steps.map((label) => (
                        <Step key={label}>
                            <StepLabel>{label}</StepLabel>
                        </Step>
                    ))}
                </Stepper>
            )}
            {!smallScreenMatched && (
                <div>
                    {activeStep === steps.length ? (
                        <div>
                            <Typography>表格以提交</Typography>
                            <Button onClick={handleReset}>重新填表</Button>
                        </div>
                    ) : (
                            <div>
                                <div className={classes.formContent}>{getStepContent(activeStep)}</div>
                                <div>
                                    <Button
                                        disabled={activeStep === 0}
                                        onClick={handleBack}
                                        className={classes.backButton}
                                    >
                                        上一步
                                </Button>
                                    <Button variant="contained" color="primary" onClick={handleNext}>
                                        {activeStep === steps.length - 1 ? '提交' : '下一步'}
                                    </Button>
                                </div>
                            </div>
                        )}
                </div>
            )}
            {smallScreenMatched && (
                <div>
                    <div className={classes.formContent}>{getStepContent(activeStep)}</div>
                    <MobileStepper
                        steps={steps.length}
                        position="static"
                        variant="text"
                        activeStep={activeStep}
                        nextButton={
                            <Button size="small" onClick={handleNext} disabled={activeStep === steps.length - 1}>
                                Next
                                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                            </Button>
                        }
                        backButton={
                            <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
                                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                                Back
                            </Button>
                        }
                        classes={classes.footer}
                    />
                </div>
            )}
        </>
    );
}

export default StepperWrapper;